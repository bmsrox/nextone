var mongoose = require('mongoose');

// Criando um schema (Pode exportar schema, boa prática)
var Schema = mongoose.Schema;

// Atribuir campos vazios para alocar espaços e não
// alterar o objeto inteiro quando o campo tiver valor
// Isso faz com que o MongoDB ganhe performance
var DisplaySchema = new Schema({

	name: { type: String, default: '', required: true },
	address: { type: String, default: '' },
	id:{ type: String, default: '' },
	showlastpassword:{ type: Boolean, default: false },
	showfullcall:{ type: Boolean, default: true },
	showalfa:{ type: Boolean, default: true },
	voice:{ type: String, default: 'beepsound' },
	numberpass:{type: Number, default:4},
	numberpoint:{type: Number, default:2},
	ia:{type:String, default:''},
	labelpassword:{type:String, default:'SENHA'},
	labelpasswordcustom:{type:String, default:''},
	labelpoint:{type:String, default:'GUICHÊ'},
	labelpointcustom:{type:String, default:''},
	layout: {type: String, default: '', required: true},
	// playlist: {
	// 	name: {type:String, default:''}, //nome da playlist
	// 	fulltime: {type:String, default:''}, // total de tempo da playlist
	// 	order: {type:String, default:0},
	// 	tracks:{
	// 		kind: {type:String, default:''}, //tipo do arquivo
	// 		url: {type:String, default:''}, //link
	// 		pathfile: {type:String, default:''}, //caminho do arquivo
	// 		time: {type:String, default:''} //tempo de execução do arquivo
	// 	}
	// },

	// rss: {
	// 	fulltime: {type:String, default:''}, // total de tempo da playlist
	// 	order: {type:String, default:0},
	// 	tracks:{
	// 		name: {type:String, default:''},
	// 		url: {type:String, default:''}, //url do RSS
	// 		time: {type:String, default:''} // tempo de execução da cada noticia
	// 	}
	// },

	layout: {
		borderColor:{type:String, default:''},
		full:{
			background: {type:String, default:''},
			color: {type:String, default:''},
			label:{
				color: {type:String, default:''}
			}
		},
		lastpass:{
			header:{
				background: {type:String, default:''},
				color: {type:String, default:''}
			},
			color: {type:String, default:''},
			label:{
				color: {type:String, default:''}
			}
		},
		header:{
			background: {type:String, default:''},
			color: {type:String, default:''}
		},
		container:{
			background: {type:String, default:''},
			color: {type:String, default:''}
		},
		sidebar:{
			box:{
				background: {type:String, default:''},
				color: {type:String, default:''},
				label:{
					color: {type:String, default:''}
				}
			}
		},
		footer:{
			background: {type:String, default:''},
			color: {type:String, default:''}
		}
	}
});

// Instanciando o model Beer = beers no mongo
module.exports = mongoose.model('Display', DisplaySchema);