var mongoose = require('mongoose');

// Criando um schema (Pode exportar schema, boa prática)
var Schema = mongoose.Schema;

// Atribuir campos vazios para alocar espaços e não
// alterar o objeto inteiro quando o campo tiver valor
// Isso faz com que o MongoDB ganhe performance
var UserSchema = new Schema({

	name: { type: String, default: '', required: true },
	login: { type: String, default: '', required: true, unique: true },
	password:{ type: String, default: '', required: true}

});

// Instanciando o model Beer = beers no mongo
module.exports = mongoose.model('User', UserSchema);