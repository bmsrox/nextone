var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/nextone');

var db = mongoose.connection;

db.on('error', function(err){
	console.log('Erro de conexao.', err)
});

db.on('open', function () {
	console.log('Conexão aberta.')
});

db.on('connected', function () {
	console.log('Conectado')
});

db.on('disconnected', function () {
	console.log('Desconectado')
});