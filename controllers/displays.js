var Model = require('./../models/display');

var msg = '';

module.exports = {
	create: function (req, res, callback) {
		// Inserindo dados no model para inserir no mongo
		var dados = req.body;
		console.log(dados);
		// Instanciando a model Model
		var model = new Model(dados);

		// Persiste a model
		model.save(function (err, data) {
			callback(err, data, res);
		});
	},
	retrieve: function(req, res, callback) {

		Model.find({}, function (err, data) {
			callback(err, data, res);
		})
	},
	findOne: function(req, res, callback) {
		var query = {_id: req.params.id};
		Model.findOne(query, function (err, data) {
			callback(err, data, res);
		});
	},
	update: function(req, res, callback) {

		var query = {_id: req.params.id};
		var mod = req.body;

		var optional = {
			upsert: false,
			multi:true
		}
		delete mod._id;

		Model.update(query, mod, function (err, data) {
			callback(err, data, res);
		});
	},
	delete: function(req, res, callback) {
		var query = {_id: req.params.id};

		// Remove as collections com query
		Model.remove(query, function (err, data) {
			callback(err, data, res);
		});
	}
}