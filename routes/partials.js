var express = require('express');
var router = express.Router();

router.get('/:name', function(req, res) {
  var name = req.params.name;
  res.render('partials/' + name);
});

router.get('/:dir/:name', function(req, res) {
  var dir = req.params.dir;
  var name = req.params.name; console.log(dir)
  res.render('partials/' + dir +'/' + name);
});

module.exports = router;