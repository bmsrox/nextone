var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('backend', { title: 'Nextone+' });
});

module.exports = router;
