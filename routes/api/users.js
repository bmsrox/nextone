var express = require('express');
var router = express.Router();
var Controller = require('./../../controllers/users');

var callback = function(err, data, res){
  if (err){
    msg = '{Erro: ' + err +'}' ;
  }
  else{
    msg = data;
  }
  console.log(msg);
  res.json(msg);
}

/* GET users listing. */
router.get('/', function(req, res){
	Controller.retrieve(req, res, callback);
});

router.get('/:id', function(req, res) {
  Controller.findOne(req, res, callback);
});

router.get('/login/:login', function(req, res) {
  Controller.findByUsername(req, res, callback);
});

router.post('/', function(req, res){
	Controller.create(req, res, callback);
});

router.put('/:id', function(req, res){
	Controller.update(req, res, callback);
});

router.delete('/:id', function(req, res){
	Controller.delete(req, res, callback);
});

module.exports = router;