var express = require('express');
var router = express.Router();
var Controller = require('./../../controllers/populate');

var callback = function(err, data, res){
  if (err){
    msg = '{Erro: ' + err +'}' ;
  }
  else{
    msg = data;
  }
  console.log(msg);
  res.json(msg);
}

router.get('/', function(req, res){
	Controller.create(req, res, callback);
});

module.exports = router;
