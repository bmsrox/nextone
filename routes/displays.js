var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res) {
  res.render('backend', { title: 'Nextone+' });
});

router.get('/:id', function(req, res) {
  res.render('backend', { title: 'Nextone+' });
});

router.get('/cadastrar', function(req, res) {
  res.render('backend', { title: 'Nextone+' });
});

router.get('/editar/:id', function(req, res) {
  res.render('backend', { title: 'Nextone+' });
});

module.exports = router;