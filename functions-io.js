module.exports = function (client, io, db){

	// var cron = require('cron');
	// var rss = require('ortoo-feedparser');

	var DisplayModel = require('./models/display');
	var baseUrl = client.handshake.headers.referer;
	var ip = client.handshake.address;

	// var loadRss = function (){
	//     //Carregar playlist e RSS para este client no WebSQL
	//     var url = 'http://rss.uol.com.br/feed/noticias.xml'
	//     rss.parseUrl(url,function (error, meta, articles){
	//     	if (error) console.error(error);
	//     	else {
	//     		client.emit('LoadRss', articles);
	//     	}
	//     });
	// }

	//coloca o ip do cliente em uma sala
	client.join(ip);
	client.emit("log","Connected to Server ... IP: " + ip);

	DisplayModel.findOne({address: ip}, function (err, data) {
			if(data != null){

				var config = data;

				if(config.id != null || config.id != '' || config.id != 'undefined'){
					client.join(config.id);
				}

			}else
				var config = null;

			client.emit("config", config);
	});

	//faz chamada de teste
	client.emit("send-password", "test");

	//REINICIA AS CONFIGURAÇÕES
	client.on('reload',function(data){
		io.sockets.in(data.address).emit('reload-config', data);
	});

	//loadRss();

	// client.on('login', function(){

	// 	var sql = "SELECT apa.id_fila, hf.rotulo FROM aux_filas_pontos_de_apresentacao apa INNER JOIN pontos_de_apresentacao pa ON (pa.id_ponto_apresentacao = apa.id_ponto_apresentacao) INNER JOIN historico_filas hf ON (hf.id_fila = apa.id_fila) WHERE pa.ip = ? ORDER BY apa.id_fila ASC";

	// 	db.query(sql,[ip],function(err, rows){
	// 		if(err) {
	// 			throw err;
	// 		}else{
	// 			var total = rows.length;

	// 			if(total>0){

	// 				//coloca o ip do cliente em uma sala
	// 				client.join(ip);
	// 				client.emit("log","Connected to Server...\nMy IP: " + ip + "\nListing queues...");

	// 				for (i = 0; i < total; i++) {

	// 					var id = rows[i].id_fila;

	// 					if(id != 'undefined' || id != null){
	// 						client.join(id);
	// 						client.emit('log', '> Display queue: ' + rows[i].rotulo);
	// 					}

	// 				}

	// 				loadRss();

	// 			}else{
	// 				client.emit('log','This IP is not configured');
	// 			}
	// 		}
	// 	});

	// });

	// //Faz a chamada da senha
	// client.on('CallPassword',function(data){
	//   io.sockets.in(data.fila).emit('ShowPassword', data);
	// });

	client.on('disconnect', function(){
	  console.log('IP %s DESCONECTADO!', ip);
	});

}