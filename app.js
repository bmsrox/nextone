var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var http = require('http');
var db = require('./db/config');

var routes = require('./routes/index');
var login = require('./routes/login');
var partials = require('./routes/partials');

var admin = require('./routes/admin');
var users = require('./routes/users');
var displays = require('./routes/displays');

var api = {};
api.displays = require('./routes/api/displays');
api.users = require('./routes/api/users');
api.populate = require('./routes/api/populate');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('port', process.env.PORT || 3001);

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/login', login);
app.use('/partials', partials);

app.use('/admin', admin);
app.use('/users', users);
app.use('/displays', displays);

//ADMIN
// for(var route in admin){
//     if(route == 'index')
//         app.use('/admin', admin[route]);
//     else
//         app.use('/admin/'+route, admin[route]);
// }

//ADMIN
for(var routeApi in api){
    app.use('/api/'+routeApi, api[routeApi]);
}


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

var server = http.Server(app);
var io = require('socket.io')(server);

io.on('connection', function (socket) {
    require('./functions-io')(socket, io, db); //importa todas as sockets do socket.io
});

//CRIA UM SOCKET UDP
var dgram = require('dgram');
var udpServer = dgram.createSocket('udp4');

udpServer.on("error", function (err) {
  console.log("server error:\n" + err.stack);
  server.close();
});

udpServer.on('listening', function () {
    var address = udpServer.address();
    console.log('Server UDP listening on ' + address.address + ":" + address.port);
});

udpServer.on('message', function (message, remote) {

    //Aki fazer a validação para enviar uma UDP para o software do Okuma

    var msg = message.toString('binary');

    console.log(remote.address + ':' + remote.port +'\n' + msg);
    io.sockets.emit('send-password', msg);

    var buffer = new Buffer(msg);
    udpServer.send(buffer, 0, buffer.length, remote.port, remote.address, function(err, bytes) {
      //udpServer.close();
    });
});

udpServer.bind(app.get('port'));

server.listen(app.get('port'), function() {
  console.log('Server HTTP listening on port ' + server.address().port);
});

