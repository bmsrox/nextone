angular.module('nextone', [
	'nextone.controllers',
	'nextone.factories',
	'nextone.services',
	'nextone.directives',
	'nextone.filters'
]).run(function(socket, Config){

	if(typeof(Storage) != "undefined") {

		socket.on('config', function(config){
			Config.setConfig(config);
		});

		var config = Config.getConfig();

		if(config === null)
			alert('Dispositivo não esta configurado corretamente!');

	} else {
		alert('Navegador não suporta localStorage!');
	}

});
