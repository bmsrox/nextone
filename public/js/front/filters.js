angular.module('nextone.filters', [])
.filter('pad', function() {
  return function(num) {
    return (num < 10 ? '0' + num : num); // coloca o zero na frente
  };
});