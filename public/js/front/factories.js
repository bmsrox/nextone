angular.module('nextone.factories', []).
factory('socket', function($rootScope) {
	var socket = io.connect(document.URL); // Connection to the server socket
	return {
		on: function(eventName, callback) { // Return callback to the actual function to manipulate it.
			socket.on(eventName, function() {
				var args = arguments;
				$rootScope.$apply(function() {
					callback.apply(socket, args);
				});
			});
		},
		 emit: function (eventName, data, callback) {
	      socket.emit(eventName, data, function () {
	        var args = arguments;
	        $rootScope.$apply(function () {
	          if (callback) {
	            callback.apply(socket, args);
	          }
	        });
	      })
	    }

	};
}).
factory('call', function(Config){

		var config = Config.getConfig();
		var obj = {};

		obj.fullscreenShow = function(scope){
			scope.fullscreenShow = true;
		}

		obj.fullscreenHide = function(scope){
			scope.fullscreenShow = false;
		}

		obj.format_password = function(data) {

			if(typeof data != "undefined" && data != ''){

				var passlabel = config.labelpassword != 'custom' ? config.labelpassword : config.labelpasswordcustom;
				var pointlabel = config.labelpoint != 'custom' ? config.labelpoint : config.labelpointcustom;

				var show = {
					password:{ label: passlabel, alfa:'', pass:'' },
					pa:{ label:pointlabel, number:''},
					queue:{ label:'' },
					client:{ name: '' }
				};

				var string = data.split('#');

				if(typeof string[0] != "undefined" && string[0] != ''){
					var new_format_pass = string[0].substr(11, string[0].length);

					//CONDIÇÃO PARA EXIBIR O ALFA
					if(config.showalfa)
						show.password.alfa = new_format_pass.substr(0,1);

					var pass = new_format_pass.substr(1,4); //recupera o numero da senha
					var number = new_format_pass.substr(5,new_format_pass.length); //recupera o numero do ponto de atendimento

					//DEFINE O NUMEROS DE CARACTERS DA SENHA
					show.password.pass = pass.substr(pass.length-config.numberpass, pass.length);

					//DEFINE O NUMEROS DE CARACTERS DO PONTO DE ATENDIMENTO
					show.pa.number = number.substr(number.length-config.numberpoint, number.length);

				}

				//DEFINE O ROTULO DO PONTO DE ATENDIMENTO VINDO DE DISPOSITIVOS EXTERNOS
				if(typeof string[1] != "undefined" && string[1] != '')
					show.pa.label = string[1];

				//DEFINE O ROTULO DA FILA VINDO DE DISPOSITIVOS EXTERNOS
				if(typeof string[2] != "undefined" && string[2] != '')
					show.queue.label = string[2];

				//DEFINE O NOME DO CLIENTE VINDO DE DISPOSITIVOS EXTERNOS
				if(typeof string[3] != "undefined"  && string[3] != '')
					show.client.name = string[3];

				return show;

			}

		}

		obj.show_password = function(data, scope) {

			//dados da senha
	        scope.labelPass = data.password.label;
	        scope.password = data.password.alfa + data.password.pass;

	        //nome da fila
	        scope.queueName = data.queue.label;

	        //rotulo e numero do ponto
	        scope.labelPoint = data.pa.label;
	        scope.point = data.pa.number;

	        //nome do cliente
	        scope.clientName = data.client.name;

			//exibe as ultimas senhas
	        scope.lastpass = data.lastpass;

		}

		obj.get_queue = function(){
			return JSON.parse(localStorage.getItem("queuePass"));
		}

		obj.set_queue = function(data) {
			localStorage.setItem("queuePass", JSON.stringify(data));
		}

		obj.save_password = function(data){

			var db = obj.get_queue();

			if(db == null || db == 'undefined' || db == '')
				db = [];

			db.push(data);

			obj.set_queue(db);
		}

		obj.get_password = function(data){

			var data = obj.format_password(data);

			if(typeof data != "undefined"){

				var pass = [
			    	data.password.label,
			    	data.password.alfa,
			    	data.password.pass,
			    	data.pa.label,
				    data.pa.number
		      	];

				obj.save_password(pass);
			}else{
				throw new Error("String 'data' is empty or doesnt exist.")
			}

		}

		obj.rss = function(articles){
			articles.forEach(function (article){
				console.log(article.title)
			});
		}

		obj.setConfig = function (config){

			localStorage.removeItem("config");

			if(config !== null){
				var config = JSON.stringify(config);
				localStorage.setItem("config", config);
			}
		}

		obj.setLastestPass = function(data){

			var newData = obj.getLastestPass();

			var totalData = newData.length;
			var total = 3;

			if(totalData > total){
				newData.splice(total, totalData);
			}

			newData.unshift(data);

			localStorage.setItem("lastPass", JSON.stringify(newData));
		}

		obj.getLastestPass = function(){

			var lastPass = localStorage.getItem("lastPass");

			if(lastPass == null || lastPass == 'undefined' || lastPass == '')
				return [];
			else
				return JSON.parse(lastPass);
		}

		obj.showLastestPass = function(){
			if(config.showlastpassword){
			    return obj.getLastestPass();
			}else{
				localStorage.removeItem("lastPass");
			}
			return [];
		}

		obj.getCallVoice = function(tracks){

			var playlist = new jPlayerPlaylist({
			  jPlayer: "#jquery_jplayer_1",
			},
			[]
			,{
			  swfPath: "js",
			  supplied: "mp3",
			  playlistOptions: {
			    autoPlay: true,
			    loopOnPrevious: false,
			    shuffleOnLoop: false
			  },
			});

			playlist.setPlaylist(tracks); //recebe o array que faz a chamada por voz

		}

		obj.validateAudioPass =  function(data){

			var path_words = '/audio/palavras/';
			var path_numbers = '/audio/numeros/';
			var path_abc = '/audio/alfabeto/';

			var string = [];
			//AUDIO BIP
			string.push({mp3:'/audio/campainha.mp3'});

			if(config.voice == 'beepwithoutsound'){
				return string; //retorna apenas o bip
			}else if(config.voice == 'beepatentionsound'){
				string.push({mp3:path_words +'atencao.mp3'}); //retorna o bip + atenção + a chamada por voz
			}else if(config.voice == 'beepnextsound'){
				string.push({mp3:path_words +'proximo.mp3'}); //retorna o bip + atenção + a chamada por voz
			}

			data.forEach(function(value, index){

				//CONFIGURAÇÔES DA INTERFACE APENAS FALA O AUDIO DE SENHA QUANDO FOR NAS CONDIÇÔES ABAIXO
				if(config.ia == 'passpoint' || config.ia == 'onlypass'){

					//ROTULO DA CHAMADA SENHA, COMANDA OU O QUE FOI DEFINIDO
					if(index == 0){
						var url = path_words + value.toLowerCase() + '.mp3';
						if(Config.validatePath(url))
							string.push({mp3:url});
					}

					//AUDIO DO ALFA
					if(index == 1){
						var letter = value.toLowerCase();
						var url = path_abc + letter +'.mp3';
						if(Config.validatePath(url))
							string.push({mp3:url});
					}
					//AUDIO DA SENHA
					if(index == 2){
						var senha = parseInt(value).toString(); // remove os zero no inicio
						var numbers = Config.extensive(senha).split(" "); //converte para um array com o numero em extenso
						//console.log(senha);
						numbers.forEach(function(number){
							var url = path_numbers + number +'.mp3';
							if(Config.validatePath(url))
								string.push({mp3:url});
						});
					}

				}

				//CONFIGURAÇÔES DA INTERFACE APENAS FALA O AUDIO DE SENHA QUANDO FOR NAS CONDIÇÔES ABAIXO
				if(config.ia == 'passpoint' || config.ia == 'onlypoint'){

					//AUDIO DO PONTO DE ATENDIMENTO OU O QUE FOI DEFINIDO
					if(index == 3){
						var label = Config.removeAcento(value).toLowerCase();
						var url = path_words + label +'.mp3';
						if(Config.validatePath(url))
							string.push({mp3:url});
					}
					//AUDIO DO NUMERO DO PONTO DE ATENDIMENTO
					if(index == 4){
						var numberPoint = parseInt(value).toString(); // remove os zero no inicio
						var point = Config.extensive(numberPoint).split(" "); //converte para um array com o numero em extenso
						//console.log(numberPoint);
						point.forEach(function(number){
							var url = path_numbers + number +'.mp3';
							if(Config.validatePath(url))
								string.push({mp3:url});
						});
					}

				}

			});

			return string;
		}

		return obj;

});

  //FAZ A CHAMADA DA SENHA POR VOZ
			 //    obj.getCallVoice(obj.validateAudioPass([
				//     	data.password.label,
				//     	data.password.alfa,
				//     	data.password.pass,
				//     	data.pa.label,
				// 	    data.pa.number
			 //      	])
			 //    );

			 //    //seta as últimas senhas chamadas
			 //   	if(config.showlastpassword){
			 //        obj.setLastestPass({
			 //        	labelPass:data.password.label,
			 //        	password:data.password.alfa + data.password.pass,
			 //        	labelPoint:data.pa.label,
			 //        	point:data.pa.number
			 //        });

			 //        data.lastpass = obj.showLastestPass();

			 //    }

				// return data;