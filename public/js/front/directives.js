angular.module('nextone.directives', [])
.directive('header', function(Config) {

  var layoutHeader = Config.setLayout('header');

    return {
      replace: true,
      link: function(scope, el, attrs) {
        scope.layoutHeader = function(){
          return layoutHeader ? layoutHeader : {}
        }
        scope.logo = '/img/logo_spider.png';
      }
    };

})
.directive('content', function(Config) {

  var layoutContent = Config.setLayout('container');

    return {
      link: function(scope, el, attrs) {
        scope.layoutContent = function(){
          return layoutContent ? layoutContent : {}
        }
      }
    };

})
.directive('sidebar', function(Config) {

  var fullpass = false;
  var fullpoint =  false;

	var config = Config.getConfig();
  var layout = Config.setLayout('sidebar');
  var border = layout['border-color'];
  layout.box['border-color'] = border;

  	if(config.ia == 'onlypass'){
      var fullpass = true;
  	}

  	if(config.ia == 'onlypoint'){
      var fullpoint =  true;
  	}

    return {
      //templateUrl: template,
      link: function(scope, el, attrs) {

        scope.interface = function(scenario) {
            if(fullpass){
              if(scenario == 'point')
                return {hide:true}
              else
                return {full:true}
            }

            if(fullpoint){
              if(scenario == 'pass')
                return {hide:true}
              else
                return {full:true}
            }
        }

        scope.layoutSidebar = function() {
          return {
            'border-color':border
          }
        }

        scope.layoutSiderBarBox = function() {
          return layout.box ? layout.box : {}
        }

        scope.layoutSiderBarBoxLabel = function(){
          return layout.box.label ? layout.box.label : {}
        }

      }
    };

})
.directive('footer', function(Config) {

  var layoutFooter = Config.setLayout('footer');

    return {
      link: function(scope, el, attrs) {
         scope.layoutFooter = function(){
          return layoutFooter ? layoutFooter : {}
        }
      }
    };

})
.directive('fullscreen', function(Config) {

  var fullpass = false;
  var fullpoint =  false;

	var config = Config.getConfig();
  var layoutFull = Config.setLayout('full');

    if(config.ia == 'onlypass'){
      var fullpass = true;
    }

    if(config.ia == 'onlypoint'){
      var fullpoint =  true;
    }

    return {

      link: function(scope, el, attrs) {

        //ESCONDE O NOME DO CLIENTE E  NOME DA FILA QUANDO CONFIGURADO PARA EXIBIR APENAS SENHA OU PA
        scope.hideOnlyPassOrPoint = function() {
          if(fullpass || fullpoint){
            return {hide:true}
          }
        }

        scope.interface = function(scenario) {
            if(fullpass){
              if(scenario == 'point')
                return {hide:true}
              else
                return {full:true}
            }

            if(fullpoint){
              if(scenario == 'pass')
                return {hide:true}
              else
                return {full:true}
            }
        }

        scope.layoutCallFullscreen = function(){
          return layoutFull ? layoutFull : {}
        }

        scope.layoutCallFullscreenLabel = function(){
          return layoutFull.label ? layoutFull.label : {}
        }

        scope.layoutCallFullscreenSubTitles = function(){
          return layoutFull.subtitles ? layoutFull.subtitles : {}
        }

      }
    };

})
// .directive('layout', function(Config) {

//   var config = Config.getConfig();

//   if(config.layout.type == 'landscape')
//     var template = 'partials/front/landscape.ejs';
//   else
//     var template = 'partials/front/portrait.ejs';

//     return {
//       restrict: 'E',
//       templateUrl: template
//     };

// })
// .directive('lastpass', function(Config) {

//   var layoutLastPass = Config.setLayout('lastpass');
//   var config = Config.getConfig();
//   var template = 'partials/front/lastpass.ejs';
//     return {
//       restrict: 'E',
//       replace:true,
//       templateUrl: template,
//       link: function(scope, el, attrs) {

//         scope.layoutLastPass = function(){
//           return layoutLastPass ? layoutLastPass : {}
//         }

//         scope.layoutLastPassHeader = function(){
//           return layoutLastPass.header ? layoutLastPass.header : {}
//         }

//         scope.layoutLastPassHeaderTitle = function(){
//           return {
//             'color': layoutLastPass.header.color
//           }
//         }

//         scope.layoutLastPassLabel = function(){
//           return layoutLastPass.label ? layoutLastPass.label : {}
//         }

//       }
//     };

// })
// .directive('clock', function() {
//   return {
//     restrict: 'EA',
//     replace: true,
//     scope: {
//       radius: '@',
//       zone: '@?',
//       lightFill: '@?',
//       darkFill: '@?'
//     },
//     templateUrl: 'partials/front/analog.clock.ejs',
//     link: function(scope, el, attrs) {
//       var drawClock, drawHands, getRad, getX, getY;
//       if (scope.lightFill == null) {
//              scope.lightFill = '#f2f2f2';
//            }
//            if (scope.darkFill == null) {
//              scope.darkFill = 'white';
//            }
//       scope.divStyle = function() {
//         return {
//           width: '100%',
//           height: '100%'
//         };
//       };

//       scope.circleStyle = function() {
//         return {
//           stroke: 'none',
//           fill: 'none',
//           "stroke-width": '1px'
//         };
//       };

//       scope.svgStyle = function(){
//           return {
//             width:"100%",
//             height:"100%",
//           }
//       };

//       scope.hour = function() {
//         return {
//           'stroke-width': '12px'
//         };
//       };

//       scope.minute = function() {
//         return {
//           'stroke-width': '8px'
//         };
//       };

//       scope.second = function() {
//         return {
//           'stroke-width': '4px',
//           'stroke': '#fae072'
//         };
//       };

//       getX = function(degrees, r, adjust, x) {
//         var adj;
//         x = x || r;
//         adj = adjust || 1;
//         return x + r * adj * Math.cos(getRad(degrees));
//       };
//       getY = function(degrees, r, adjust, y) {
//         var adj;
//         y = y || r;
//         adj = adjust || 1;
//         return y + r * adj * Math.sin(getRad(degrees));
//       };
//       getRad = function(degrees) {
//         var adjust;
//         adjust = Math.PI / 2;
//         return (degrees * Math.PI / 180) - adjust;
//       };
//       drawClock = function() {
//         return drawHands();
//       };
//       drawHands = function() {
//         var $circle, H_HAND_SIZE, M_HAND_SIZE, S_HAND_SIZE, dark, drawHand, fillColor, hour, hour24, r, strokeColor, t;
//         S_HAND_SIZE = 0.95;
//         M_HAND_SIZE = 0.85;
//         H_HAND_SIZE = 0.55;
//         t = scope.zone ? moment.tz(new Date(), scope.zone) : moment();
//         r = 100;
//         $circle = el.find('circle');
//         hour24 = Number(t.format('H'));
//         dark = hour24 >= 18 || hour24 < 6;
//         fillColor = dark ? scope.darkFill : scope.lightFill;
//         strokeColor = dark ? 'white' : 'black';
//         $circle.css('fill', fillColor);
//         el.find('line').not('.secondhand').css('stroke', strokeColor);
//         drawHand = function(hand, value, size, degrees) {
//           var deg, x2, y2;
//           deg = degrees * value;
//           x2 = getX(deg, r, size, r);
//           y2 = getY(deg, r, size, r);
//           hand.attr("x1", r);
//           hand.attr("y1", r);
//           hand.attr("x2", x2);
//           return hand.attr("y2", y2);
//         };
//         hour = t.hour() + t.minute() / 60;
//         drawHand($(el).find(".secondhand"), t.second(), S_HAND_SIZE, 6);
//         drawHand($(el).find(".minutehand"), t.minute(), M_HAND_SIZE, 6);
//         return drawHand($(el).find(".hourhand"), hour, H_HAND_SIZE, 30);
//       };
//       drawClock();
//       return setInterval(drawHands, 1000);
//     }
//   };
// });
// .directive('clock', ["$timeout", function($timeout){
//   return {
//     restrict: 'E',
//     templateUrl: 'partials/front/clock.digital.ejs',
//     controller: function($scope, $element) {
//       $scope.date = new Date();

//       var tick = function() {
//         $scope.date = new Date();
//         $timeout(tick, 1000);
//       };
//       $timeout(tick, 1000);
//     }
//   }

// }]);
// .directive('clock', function() {

// 	var template = 'partials/front/clock.analog.ejs';
// 	var template = 'partials/front/clock.digital.ejs';
//     return {
//       restrict: 'E',
//       template: '<div>Exibir relogio</div>'
//     };

// });