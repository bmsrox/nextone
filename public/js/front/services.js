angular.module('nextone.services', []).
service('Config', function($http){

	//+ Carlos R. L. Rodrigues
	//@ http://jsfromhell.com/string/extenso [rev. #3]
	String.prototype.extenso = function(c){
		var ex = [
			["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"],
			["10", "20", "30", "40", "50", "60", "70", "80", "90"],
			["100", "100to", "200", "300", "400", "500", "600", "700", "800", "900"],
			["mil", "milhao", "bilhao", "trilhao", "quadrilhao", "quintilhao", "sextilhao", "setilhao", "octilhao", "nonilhao", "decilhao", "undecilhao", "dodecilhao", "tredecilhao", "quatrodecilhao", "quindecilhao", "sedecilhao", "septendecilhao", "octencilhao", "nonencilhao"]
		];
		var a, n, v, i, n = this.replace(c ? /[^,\d]/g : /\D/g, "").split(","), e = " e ", $ = "real", d = "centavo", sl;
		for(var f = n.length - 1, l, j = -1, r = [], s = [], t = ""; ++j <= f; s = []){
			j && (n[j] = (("." + n[j]) * 1).toFixed(2).slice(2));
			if(!(a = (v = n[j]).slice((l = v.length) % 3).match(/\d{3}/g), v = l % 3 ? [v.slice(0, l % 3)] : [], v = a ? v.concat(a) : v).length) continue;
			for(a = -1, l = v.length; ++a < l; t = ""){
				if(!(i = v[a] * 1)) continue;
				i % 100 < 20 && (t += ex[0][i % 100]) ||
				i % 100 + 1 && (t += ex[1][(i % 100 / 10 >> 0) - 1] + (i % 10 ? e + ex[0][i % 10] : ""));
				s.push((i < 100 ? t : !(i % 100) ? ex[2][i == 100 ? 0 : i / 100 >> 0] : (ex[2][i / 100 >> 0] + e + t)) +
				((t = l - a - 2) > -1 ? " " + (i > 1 && t > 0 ? ex[3][t].replace("ao", "oes") : ex[3][t]) : ""));
			}
			a = ((sl = s.length) > 1 ? (a = s.pop(), s.join(" ") + e + a) : s.join("") || ((!j && (n[j + 1] * 1 > 0) || r.length) ? "" : ex[0][0]));
			a && r.push(a + (c ? (" " + (v.join("") * 1 > 1 ? j ? d + "s" : (/0{6,}$/.test(n[0]) ? "de " : "") + $.replace("l", "is") : j ? d : $)) : ""));
		}
		return r.join(e);
	}

	this.extensive = function(value){
		return value.extenso();
	}

	this.removeAcento = function(strToReplace) {
		str_acento= "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
		str_sem_acento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
		var nova="";
		for (var i = 0; i < strToReplace.length; i++) {
			if (str_acento.indexOf(strToReplace.charAt(i)) != -1) {
				nova+=str_sem_acento.substr(str_acento.search(strToReplace.substr(i,1)),1);
			} else {
				nova+=strToReplace.substr(i,1);
			}
		}
		return nova;
	}

	this.validatePath = function(url){
		return true;
	}

	this.setConfig = function(config){
		localStorage.removeItem("config");

		if(config !== null){
			var config = JSON.stringify(config);
			localStorage.setItem("config", config);
		}
	}

	this.getConfig = function(){
		var config = localStorage.getItem("config");

		if(config != null)
			config = JSON.parse(config);
		else{
			config = null;
		}

		return config;
	}

	this.unCamelCase = function(input) {
	    return input.replace(/([a-z])([A-Z])/, "$1-$2").toLowerCase();
	}

	this.setLayout = function(directive){

		var config = this.getConfig();

		if(config !== null || config !== undefined){

			var layout = config['layout'];
			var border = layout.borderColor;

			var style = {};
			style['border-color'] = border;
			for(var key in layout[directive]){
				style[key] = layout[directive][key];
			}

			return style;

		}

		return false;
	}

});

				// if(typeof layout[css] == 'string'){

				// 	if(layout[css] != '')
				// 		style[this.unCamelCase(css)] = layout[css];

				// }

				// if(typeof layout[css] == 'object'){

				// 	var style2 = {};

				// 	for(var css2 in layout[css]){
				// 		if(layout[css][css2] != ''){
				// 			style2[this.unCamelCase(css2)] = layout[css][css2];
				// 			style2['border-color'] =  config['layout'].borderColor;
				// 		}
				// 	}

				// 	style[css] = style2;
				// }