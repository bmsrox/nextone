angular.module('backend', [
  'backend.controllers',
  'backend.services',
  'backend.filters',
  'ngRoute',
  'colorpicker.module'
]).
run(function($window, $rootScope, Login){

  if(!Login.check()){
    $window.location.href = '/login';
    return false;
  }else{
    Login.expire();
    var data = Login.getDataLogin();
    $rootScope.username = data.name;
  }

}).
config(function ($routeProvider, $locationProvider) {
  $routeProvider.
    when('/admin', {
      templateUrl: '/partials/backend/dashboard'
    }).
    when('/displays', {
      templateUrl: '/partials/displays/list',
      controller: 'ListDisplayController'
    }).
    when('/displays/cadastrar', {
      templateUrl: '/partials/displays/create',
      controller: 'CreateDisplayController'
    }).
    when('/displays/:id', {
      templateUrl: '/partials/displays/single',
      controller: 'SingleDisplayController'
    }).
    when('/displays/editar/:id', {
      templateUrl: '/partials/displays/edit',
      controller: 'EditDisplayController'
    }).
    when('/users', {
      templateUrl: '/partials/user/list',
      controller: 'ListUserController'
    }).
    when('/users/cadastrar', {
      templateUrl: '/partials/user/create',
      controller: 'CreateUserController'
    }).
    when('/users/:id', {
      templateUrl: '/partials/user/single',
      controller: 'SingleUserController'
    }).
    when('/users/editar/:id', {
      templateUrl: '/partials/user/edit',
      controller: 'EditUserController'
    }).

    otherwise({
      redirectTo: '/admin'
    });

  $locationProvider.html5Mode(true);
}).
factory('socket', function($rootScope) {
  var socket = io.connect('http://'+location.host); // Connection to the server socket
  return {
    on: function(eventName, callback) { // Return callback to the actual function to manipulate it.
      socket.on(eventName, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          callback.apply(socket, args);
        });
      });
    },
     emit: function (eventName, data, callback) {
        socket.emit(eventName, data, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            if (callback) {
              callback.apply(socket, args);
            }
          });
        })
      }

  };
});