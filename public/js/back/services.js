angular.module('backend.services', []).
service('Login', function(){

	var data = JSON.parse(sessionStorage.getItem("login"));

	this.check = function() {
		if(!data)
			return false;

		return true;
	}

	this.expire = function() {
		var expire = data.expire;
		if(expire != 0){
			setTimeout(function(){ sessionStorage.removeItem("login"); }, expire);
		}
	}

	this.getDataLogin = function() {
		return data;
	}
})