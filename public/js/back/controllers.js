angular.module('backend.controllers', []).
controller('LogoutController',['$window', '$scope', function($window, $scope){
	$scope.logout = function(){
		sessionStorage.removeItem("login");
		$window.location.href = '/login';
	}
}]).
controller('DashboardController',['$scope', function($scope){
	$scope.page = 'Dashboard';
}])
.controller('ListDisplayController', ['$scope', '$http', '$routeParams',function($scope, $http, $routeParams) {
	$http({
	  method: 'GET',
	  url: '/api/displays'
	}).
	success(function (data, status, headers, config) {
	  $scope.displays = data;
	}).
	error(function (data, status, headers, config) {
	  $scope.msg = 'Error!';
	});

	$scope.remove = function (display) {
	  if(confirm('Deseja mesmo deletar o display (' + display.name + ')?')){
	    var id = display._id;
	    $http({
	      method: 'DELETE',
	      url: '/api/displays/' + id
	    }).
	    success(function (data, status, headers, config) {
	      console.log(data);
	      $scope.msg = 'Display "' + display.name + '" removido com sucesso!';
	      var index = $scope.displays.indexOf(display);
	      $scope.displays.splice(index, 1);
	    }).
	    error(function (data, status, headers, config) {
	      $scope.msg = 'Error!';
	    });
	  }
	}
}])
.controller('CreateDisplayController', ['$scope', '$http', '$routeParams', '$location',function($scope, $http, $routeParams, $location) {

	$scope.saveForm = function(){

		$http({
			method: 'POST',
			url: '/api/displays',
			data: $scope.display
		}).
		success(function (data, status, headers, config) {
			console.log(data);
			alert('Display "' + $scope.display.name + '" cadastrado com sucesso!');
			$location.path("/displays/"+data._id);
		}).
		error(function (data, status, headers, config) {
			alert('Error!');
		});
	}

	$scope.display = {
		labelpassword:'SENHA',
		ia: 'passpoint',
		showfullcall: true,
		showalfa:true,
		numberpass: 4,
		numberpoint: 2,
		labelpoint: 'GUICHÊ',
		voice:'beepsound',
		layout: {
			borderColor:'#ffffff',
			full:{
				background: '#0086f5',
				color: '#ffffff',
				label:{
					color: '#fae072'
				}
			},
			lastpass:{
				header:{
					background: '#ffffff',
					color: '#0086f5'
				},
				color: '#ffffff',
				label:{
					color: '#fae072'
				}
			},
			header:{
				background: '#00569f',
				color: '#ffffff'
			},
			container:{
				background: '#0086f5',
				color: '#ffffff'
			},
			sidebar:{
				box:{
					background: '#0086f5',
					color: '#ffffff',
					label:{
						color: '#fae072'
					}
				}
			},
			footer:{
				background: '#00569f',
				color: '#ffffff'
			}
		}
	};

}])
.controller('EditDisplayController', ['$scope', '$http', '$routeParams', '$location', 'socket',function($scope, $http, $routeParams, $location, socket) {

	var id = $routeParams.id;

	$http({
	  method: 'GET',
	  url: '/api/displays/'+id
	}).
	success(function (data, status, headers, config) {
	  $scope.display = data;
	}).
	error(function (data, status, headers, config) {
	  $scope.msg = 'Error!';
	});


	$scope.saveForm = function(){

		if($scope.display.labelpassword != 'custom')
			$scope.display.labelpasswordcustom = '';

		if($scope.display.labelpoint != 'custom')
			$scope.display.labelpointcustom = '';

		socket.emit('reload',$scope.display); //REINICIA AS CONFIGURAÇÕES

		$http({
		  method: 'PUT',
		  url: '/api/displays/'+id,
		  data: $scope.display
		}).
		success(function (data, status, headers, config) {
		  console.log(data);
		  alert('Display "' + $scope.display.name + '" alterado com sucesso!');

		  $location.path("/displays/"+id);
		}).
		error(function (data, status, headers, config) {
		  alert('Error!');
		});
	}
}])
.controller('SingleDisplayController', ['$scope', '$http', '$routeParams',function($scope, $http, $routeParams) {

	var id = $routeParams.id;

	$http({
	  method: 'GET',
	  url: '/api/displays/'+id
	}).
	success(function (data, status, headers, config) {
	  console.log(data);

	  delete data.__v;
	  delete data.layout;
	  delete data.playlist;
	  delete data.rss;

	  $scope.display = data;
	}).
	error(function (data, status, headers, config) {
	  $scope.msg = 'Error!';
	});

}]).
controller('ListUserController', ['$scope', '$http', '$routeParams',function($scope, $http, $routeParams) {
	$http({
	  method: 'GET',
	  url: '/api/users'
	}).
	success(function (data, status, headers, config) {
	  $scope.users = data;
	}).
	error(function (data, status, headers, config) {
	  $scope.msg = 'Error!';
	});

	$scope.remove = function (user) {
	  if(confirm('Deseja mesmo deletar o usuário (' + user.name + ')?')){
	    var id = user._id;
	    $http({
	      method: 'DELETE',
	      url: '/api/users/' + id
	    }).
	    success(function (data, status, headers, config) {
	      console.log(data);
	      $scope.msg = 'Usuário "' + user.name + '" removido com sucesso!';
	      var index = $scope.users.indexOf(user);
	      $scope.users.splice(index, 1);
	    }).
	    error(function (data, status, headers, config) {
	      $scope.msg = 'Error!';
	    });
	  }
	}
}])
.controller('CreateUserController', ['$scope', '$http', '$routeParams', '$location',function($scope, $http, $routeParams, $location) {

	$scope.saveForm = function(){

		$http({
			method: 'POST',
			url: '/api/users',
			data: $scope.user
		}).
		success(function (data, status, headers, config) {
			console.log(data);
			alert('Usuário "' + $scope.user.name + '" cadastrado com sucesso!');
			$location.path("/users/"+data._id);
		}).
		error(function (data, status, headers, config) {
			alert('Error!');
		});
	}

}])
.controller('EditUserController', ['$scope', '$http', '$routeParams', '$location', 'socket',function($scope, $http, $routeParams, $location, socket) {

	var id = $routeParams.id;

	$http({
	  method: 'GET',
	  url: '/api/users/'+id
	}).
	success(function (data, status, headers, config) {
	  $scope.user = data;
	}).
	error(function (data, status, headers, config) {
	  $scope.msg = 'Error!';
	});

	$scope.saveForm = function(){

		$http({
		  method: 'PUT',
		  url: '/api/users/'+id,
		  data: $scope.user
		}).
		success(function (data, status, headers, config) {
		  console.log(data);
		  alert('Usuário "' + $scope.user.name + '" alterado com sucesso!');

		  $location.path("/users/"+id);
		}).
		error(function (data, status, headers, config) {
		  alert('Error!');
		});
	}
}])
.controller('SingleUserController', ['$scope', '$http', '$routeParams',function($scope, $http, $routeParams) {

	var id = $routeParams.id;

	$http({
	  method: 'GET',
	  url: '/api/users/'+id
	}).
	success(function (data, status, headers, config) {
	  console.log(data);

	  delete data.__v;
	  delete data.password;

	  $scope.user = data;
	}).
	error(function (data, status, headers, config) {
	  $scope.msg = 'Error!';
	});

}]);