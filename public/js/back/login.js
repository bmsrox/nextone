angular.module('login', [
  'backend.services'
]).
run(function($window, Login){
  if(Login.check()){
    $window.location.href = '/admin';
  }
})
.controller('LoginController', ['$scope', '$http', '$window',function($scope, $http, $window) {

  $scope.submit = function(){

      $http({
        method: 'GET',
        url: '/api/users/login/'+$scope.user.login
      }).
      success(function (data, status, headers, config) {
         if(data.password === $scope.user.password){

          var expire = 7200000;

          if($scope.user.remember)
             expire = 0;

          sessionStorage.setItem("login", JSON.stringify({name: data.name, username:data.login, expire:expire}));

          $window.location.href = '/admin';
        }else{
          alert('Login e/ou senha incorretos!');
        }
      }).
      error(function (data, status, headers, config) {
        $scope.msg = 'Error!';
      });

  }

}]);